import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/team/:id",
    name: "Team",
    component: () =>
      import(/* webpackChunkName: "team" */ "../views/Team.vue"),
  },
  {
    path: "/team-detail/:id",
    name: "TeamDetail",
    component: () =>
      import(/* webpackChunkName: "team-detail" */ "../views/TeamDetail.vue"),
  },


];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
