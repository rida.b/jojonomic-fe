const path = require('path');

module.exports = {
	devServer: {
	  proxy: {
			"/api": {
				target: "https://api.football-data.org",
				secure: false,
				logLevel: "debug",
				pathRewrite: {
					"^/api": "",
				},
				changeOrigin: true,
			},
        }
    }
}